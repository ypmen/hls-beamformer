#include <iostream>

#include "spead_recv.h"

int main()
{
	hls::stream<pkt_gbe> in;
	hls::stream<pkt_pol> out;

	ap_uint<64> spead_header = SPEAD_HEADER;
	ap_uint<64> item_heap_id = ((ap_uint<64>)0x8001 << 48) + 0;
	ap_uint<64> item_heap_size = ((ap_uint<64>)0x8002 << 48) + PACKET_LENGTH;
	ap_uint<64> item_heap_offset = ((ap_uint<64>)0x8003 << 48) + 0;
	ap_uint<64> item_packet_payload_length = ((ap_uint<64>)0x8004 << 48) + PACKET_LENGTH;
	ap_uint<64> item_timestamp = ((ap_uint<64>)ITEM_ID_TIMESTAMP << 48) + 0;
	ap_uint<64> item_clipping_cnt = ((ap_uint<64>)ITEM_ID_CLIPPING_CNT << 48) + 0;
	ap_uint<64> item_error_vector = ((ap_uint<64>)ITEM_ID_ERROR_VECTOR << 48) + 0;
	ap_uint<64> item_scale_vector = ((ap_uint<64>)ITEM_ID_SCALE_FACTOR << 48) + 0;
	ap_uint<64> item_order_vector = ((ap_uint<64>)ITEM_ID_ORDER_VECTOR << 48) + 0;

	pkt_gbe word_out;
	for (size_t k=0; k<4; k++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL/2; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT*2; ielement++)
			{
				word_out.data(63, 0) = spead_header;
				word_out.data(127, 64) = item_heap_id + (ichannel*NELEMENT + ielement);
				word_out.data(191, 128) = item_heap_size;
				word_out.data(255, 192) = item_heap_offset;
				word_out.data(319, 256) = item_packet_payload_length;
				word_out.data(383, 320) = item_timestamp + (k * PACKET_LENGTH / 2);
				word_out.data(447, 384) = item_clipping_cnt;
				word_out.data(511, 448) = item_error_vector;

				word_out.keep = -1;
				word_out.last = 0;
				in.write(word_out);

				ap_uint<48> order_vector;
				order_vector(15, 0) = ielement;
				order_vector(31, 16) = ichannel;
				order_vector(47, 32) = 0;

				word_out.data(63, 0) = item_scale_vector;
				word_out.data(127, 64) = item_order_vector + order_vector;
				word_out.data(511, 128) = 0;

				word_out.keep = -1;
				word_out.last = 0;
				in.write(word_out);

				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					word_out.data = ichannel * NELEMENT * NPACKET_PER_BLOCK + ielement * NPACKET_PER_BLOCK + ipacket;
					if (ipacket == NPACKET_PER_BLOCK - 1)
					{
						word_out.keep(15, 0) = -1;
						word_out.keep(63, 16) = 0;
						word_out.last = 1;
					}
					else
					{
						word_out.keep = -1;
						word_out.last = 0;
					}
					in.write(word_out);
				}
			}
		}
	}

	bool reset = false;
	for (size_t k=0; k<4; k++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL/2; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT*2; ielement++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK+2; ipacket++)
				{
					krnl_spead_recv(in, out, reset);
				}
			}
		}
	}

	pkt_pol word_in;

	for (size_t k=0; k<4; k++)
	{
		std::cout<<k<<std::endl;
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT; ielement++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					out.read(word_in);
					assert(word_in.user(63, 0).to_ulong() == k * NPACKET_PER_BLOCK + ipacket);
					assert(word_in.user(95, 64).to_uint() == ielement);
					assert(word_in.user(127, 96).to_uint() == ichannel);
				}
			}
		}
	}
}
