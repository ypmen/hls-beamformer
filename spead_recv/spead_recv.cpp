#include "spead_recv.h"

void map(
		ap_uint<48> &timestamp,
		ap_uint<16> &element_id,
		ap_uint<16> &channel_id,
		ap_uint<128> &user
)
{
#pragma HLS INLINE

	ap_uint<64> ipacket = timestamp / (DWIDTH / (2 * W));
	ap_uint<32> ielement = element_id % NELEMENT;
	ap_uint<32> ichannel = element_id / NELEMENT + channel_id * 2;

	user(63, 0) = ipacket;
	user(95, 64) = ielement;
	user(127, 96) = ichannel;
}

ap_uint<64> byte_reverse(ap_uint<64> a)
{
#pragma HLS INLINE

	ap_uint<64> b;

	b(7, 0) = a(63, 56);
	b(15, 8) = a(55, 48);
	b(23, 16) = a(47, 40);
	b(31, 24) = a(39, 32);
	b(39, 32) = a(31, 24);
	b(47, 40) = a(23, 16);
	b(55, 48) = a(15, 8);
	b(63, 56) = a(7, 0);

	return b;
}

void krnl_spead_recv(
		hls::stream<pkt_gbe> &in,
		hls::stream<pkt_pol> &out,
		bool &reset
)
{
#pragma HLS INTERFACE axis port = in
#pragma HLS INTERFACE axis port = out
#pragma HLS INTERFACE mode=ap_ctrl_none port=return
#pragma HLS PIPELINE II=1

	static unsigned int counter = 0;
	static bool error = false;

	static ap_uint<64> spead_header;
	static ap_uint<64> item_heap_id;
	static ap_uint<64> item_heap_size;
	static ap_uint<64> item_heap_offset;
	static ap_uint<64> item_packet_payload_length;
	static ap_uint<64> item_timestamp;
	static ap_uint<64> item_clipping_cnt;
	static ap_uint<64> item_error_vector;
	static ap_uint<64> item_scale_vector;
	static ap_uint<64> item_order_vector;

	static ap_uint<48> timestamp = 0;
	static ap_uint<16> element_id = 0;
	static ap_uint<16> channel_id = 0;
	static ap_uint<16> hardware_id = 0;
	static ap_uint<48> heap_id = 0;

	static pkt_gbe word_in;
	static pkt_pol word_out;

	static ap_uint<DWIDTH> data_cache;

	if (!in.empty())
	{
		in.read(word_in);

		if (counter == 0)
		{
			spead_header = word_in.data(63, 0);
			item_heap_id = word_in.data(127, 64);
			item_heap_size = word_in.data(191, 128);
			item_heap_offset = word_in.data(255, 192);
			item_packet_payload_length = word_in.data(319, 256);

			item_timestamp = word_in.data(383, 320);
			timestamp = byte_reverse(item_timestamp)(47, 0);
			item_clipping_cnt = word_in.data(447, 384);
			item_error_vector = word_in.data(511, 448);

			if (spead_header != SPEAD_HEADER)
				error = true;
			else
				error = false;
		}
		else if (counter == 1)
		{
			item_scale_vector = word_in.data(63, 0);
			item_order_vector = word_in.data(127, 64);
			element_id = byte_reverse(item_order_vector)(15, 0);
			channel_id = byte_reverse(item_order_vector)(31, 16);
			hardware_id = byte_reverse(item_order_vector)(47, 32);

			data_cache(255, 0) = word_in.data(511, 256);
		}
		else
		{
			if (!error)
			{
				word_out.data(255, 0) = data_cache(255, 0);
				word_out.data(511, 256) = word_in.data(255, 0);
				word_out.last = word_in.last;
				word_out.keep = -1;

				map(timestamp, element_id, channel_id, word_out.user);

				out.write(word_out);

				timestamp += DWIDTH / (2 * W);

				data_cache(255, 0) = word_in.data(511, 256);
			}
			else
			{
				counter = 0;
			}
		}

		if (word_in.last == 1)
			counter = 0;
		else
			counter++;
	}
}
