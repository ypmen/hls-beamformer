#include <ap_axi_sdata.h>
#include <hls_stream.h>

#define DWIDTH 512
#define W 8

typedef ap_axiu<DWIDTH, 128, 0, 0> pkt_pol;
typedef ap_axiu<DWIDTH, 32, 8, 0> pkt_gbe;

#define NCHANNEL 32
#define NELEMENT 32

#define NPACKET_PER_BLOCK 64

#define PACKET_LENGTH (NPACKET_PER_BLOCK*(DWIDTH/W))

#define SPEAD_HEADER 0xb000000006020453

#define ITEM_ID_TIMESTAMP 0x1680
#define ITEM_ID_CLIPPING_CNT 0x1780
#define ITEM_ID_ERROR_VECTOR 0x1880
#define ITEM_ID_SCALE_FACTOR 0x1980
#define ITEM_ID_ORDER_VECTOR 0x2080
#define ITEM_ID_RESERVED 0x2180
#define ITEM_ID_PAYLOAD 0x2280

#define ITEMID_NULL 0x0080

void krnl_spead_recv(
		hls::stream<pkt_gbe> &in,
		hls::stream<pkt_pol> &out,
		bool &reset
);
