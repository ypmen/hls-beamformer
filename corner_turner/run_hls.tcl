open_project -reset krnl_corner_turner
add_files "krnl_corner_turner.cpp krnl_corner_turner.h"
add_files -tb krnl_corner_turner_tb.cpp
set_top krnl_corner_turner
open_solution -reset solution1 -flow_target vitis
set_part {xcu55c-fsvh2892-2L-e}
create_clock -period 5
set_clock_uncertainty 0


csynth_design

export_design -format xo -output ../xo/krnl_corner_turner.xo
