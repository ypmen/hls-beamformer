#define AP_INT_MAX_W 2048

#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>

#define DWIDTH 512

typedef ap_axiu<DWIDTH, 128, 0, 0> packet;

#define W 8

#define NCHANNEL 16
#define NPOL 2
#define NELEMENT 32

#define NPACKET_PER_BLOCK 64

#define NSAMPLE_PER_PACKET (DWIDTH/(2*W))
#define RAM_DEPTH (2*NPACKET_PER_BLOCK*NELEMENT)

typedef struct {
	ap_int<W> real;
	ap_int<W> imag;
} complex_t;

typedef struct {
	ap_uint<64> sample_id;
	ap_uint<16> element_id;
	ap_uint<16> frequency_id;
} user_t;

void krnl_corner_turner(
		hls::stream<packet> &in,
		hls::stream<packet> &out
);
