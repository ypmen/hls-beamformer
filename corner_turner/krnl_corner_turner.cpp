#include "krnl_corner_turner.h"
#include <assert.h>

void push(
		hls::stream<packet> &in,
		ap_uint<2*W> ram_data[NSAMPLE_PER_PACKET][RAM_DEPTH],
		user_t ram_user[RAM_DEPTH]
)
{
#pragma HLS INLINE
	static unsigned int count = 0;

	packet pkt;

	in.read(pkt);

	unsigned int element_id = pkt.user(95, 64);
	unsigned long int packet_id = pkt.user(47, 0);
	unsigned int frequency_id = pkt.user(127, 96);

	unsigned int ihalf = count / (NELEMENT * NPACKET_PER_BLOCK) % 2;
	unsigned int ielement_block = element_id / NSAMPLE_PER_PACKET;
	unsigned int ipacket = count % NPACKET_PER_BLOCK;

	unsigned int col0 = ihalf * (RAM_DEPTH / 2) + ielement_block * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET + ipacket * NSAMPLE_PER_PACKET;
	unsigned int row0 = (element_id + col0) % NSAMPLE_PER_PACKET;

	unsigned int idx = col0 + element_id % NSAMPLE_PER_PACKET;
	ram_user[idx].element_id = ielement_block * NSAMPLE_PER_PACKET + (NSAMPLE_PER_PACKET-idx%NSAMPLE_PER_PACKET) % NSAMPLE_PER_PACKET;
	ram_user[idx].sample_id = packet_id * NSAMPLE_PER_PACKET + element_id % NSAMPLE_PER_PACKET;
	ram_user[idx].frequency_id = frequency_id;

	for (int k=0; k<NSAMPLE_PER_PACKET; k++)
	{
		unsigned int icol = col0 + k;
		unsigned int irow = (element_id + k) % NSAMPLE_PER_PACKET;

		ram_data[irow][icol] = pkt.data(k*2*W+2*W-1, k*2*W);
	}

	count++;
}

void pop(
		ap_uint<2*W> ram_data[NELEMENT][RAM_DEPTH],
		user_t ram_user[RAM_DEPTH],
		hls::stream<packet> &out
)
{
#pragma HLS INLINE
	static unsigned int boot_count = 0;
	static unsigned int count = 0;
	if (boot_count < NELEMENT*NPACKET_PER_BLOCK)
	{
		boot_count++;
		return;
	}

	unsigned int col0 = count % RAM_DEPTH;

	unsigned int element0 = ram_user[col0].element_id;
	unsigned long int sample_id = ram_user[col0].sample_id;
	unsigned int frequency_id = ram_user[col0].frequency_id;

	user_t user;
	user.sample_id = sample_id;
	user.frequency_id = frequency_id;

	packet pkt;
	pkt.keep = -1;
	pkt.user(63, 0) = user.sample_id;
	pkt.user(95, 64) = 0;
	pkt.user(127, 96) = user.frequency_id;

	for (int k=0; k<NSAMPLE_PER_PACKET; k++)
	{
		unsigned int idx = (NELEMENT - element0 + k) % NSAMPLE_PER_PACKET;
		pkt.data(k*2*W+2*W-1, k*2*W) = ram_data[idx][col0];
	}

	out.write(pkt);

	count++;
}

void krnl_corner_turner(
		hls::stream<packet> &in,
		hls::stream<packet> &out
)
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS PIPELINE II=1

#ifndef __SYNTHESIS__
	assert(NELEMENT >= NSAMPLE_PER_PACKET);
#endif

	static ap_uint<2*W> ram_data[NSAMPLE_PER_PACKET][RAM_DEPTH];
#pragma HLS ARRAY_PARTITION dim=1 type=complete variable=ram_data
	static user_t ram_user[RAM_DEPTH];

	push(in, ram_data, ram_user);
	pop(ram_data, ram_user, out);
}
