#include <cstdlib>
#include <iostream>
//#include <ctime>

#include "krnl_corner_turner.h"

#define NPACKET_PER_ELEMENT_CHANNEL 4
#define NSAMPLE_PER_PACKRT (NPACKET_PER_BLOCK*DWIDTH/(2*W))

int main()
{
	//std::srand(std::time(nullptr));

	complex_t *in = new complex_t [NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT];
	complex_t *out = new complex_t [NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NSAMPLE_PER_PACKRT*NELEMENT];
	complex_t *outRef = new complex_t [NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NSAMPLE_PER_PACKRT*NELEMENT];

	for (int iblock=0; iblock<NPACKET_PER_ELEMENT_CHANNEL; iblock++)
	{
		for (int ichannel=0; ichannel<NCHANNEL*NPOL; ichannel++)
		{
			for (int ielement=0; ielement<NELEMENT; ielement++)
			{
				for (int isample=0; isample<NSAMPLE_PER_PACKRT; isample++)
				{
					in[iblock*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT +
					   ichannel*NELEMENT*NSAMPLE_PER_PACKRT +
					   ielement*NSAMPLE_PER_PACKRT +
					   isample].real =
					outRef[iblock*NCHANNEL*NPOL*NSAMPLE_PER_PACKRT*NELEMENT +
						   ichannel*NSAMPLE_PER_PACKRT*NELEMENT +
						   isample*NELEMENT +
						   ielement].real = iblock + ichannel + ielement + isample;

					in[iblock*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT +
					   ichannel*NELEMENT*NSAMPLE_PER_PACKRT +
					   ielement*NSAMPLE_PER_PACKRT +
					   isample].imag =
					outRef[iblock*NCHANNEL*NPOL*NSAMPLE_PER_PACKRT*NELEMENT +
						   ichannel*NSAMPLE_PER_PACKRT*NELEMENT +
						   isample*NELEMENT +
						   ielement].imag = 0;
				}
			}
		}
	}

	hls::stream<packet> ins;
	hls::stream<packet> outs;

	packet pkt;
	ap_uint<DWIDTH> *pin = (ap_uint<DWIDTH> *)in;
	for (int iblock=0; iblock<NPACKET_PER_ELEMENT_CHANNEL; iblock++)
	{
		for (int ichannel=0; ichannel<NCHANNEL*NPOL; ichannel++)
		{
			for (int ielement=0; ielement<NELEMENT; ielement++)
			{
				for (int isample=0; isample<NSAMPLE_PER_PACKRT; isample+=DWIDTH/(2*W))
				{
					pkt.data = *pin;
                    pkt.user(63, 48) = 0;
                    pkt.user(47, 0) = (iblock * NSAMPLE_PER_PACKRT + isample) / (DWIDTH/(2*W));
                    pkt.user(95, 64) = ielement;
                    pkt.user(127, 96) = ichannel;
					ins.write(pkt);
					pin++;
				}
			}
		}
	}

	for (int k=0; k<NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT; k+=DWIDTH/(2*W))
		krnl_corner_turner(ins, outs);

	ap_uint<DWIDTH> *pout = (ap_uint<DWIDTH> *)out;
	for (int k=0; k<NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT-RAM_DEPTH*NSAMPLE_PER_PACKET/2; k+=DWIDTH/(2*W))
	{
		outs.read(pkt);
		*pout = pkt.data;
		pout++;
	}

	size_t errcnt = 0;
	for (int k=0; k<NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NPOL*NELEMENT*NSAMPLE_PER_PACKRT/2-(NELEMENT*NPACKET_PER_BLOCK*DWIDTH/(2*W)); k++)
	{
		if (out[k].real != outRef[k].real or out[k].imag != outRef[k].imag)
		{
			errcnt++;
		}
	}

	delete [] in;
	delete [] out;
	delete [] outRef;

	if (errcnt)
	{
		std::cout<<"Test failed!"<<" "<<errcnt<<std::endl;
		return 1;
	}
	else
	{
		std::cout<<"Test passed!"<<std::endl;
		return 0;
	}
}
