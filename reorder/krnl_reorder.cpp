#include "krnl_reorder.h"

void control(
		unsigned int pc_packet_id,
		hls::stream<long int> &read_block_counter_s
)
{
	static long int read_block_counter = -1;

	long int write_packet_pointer = pc_packet_id;
	long int read_packet_pointer = read_block_counter * NPACKET_PER_BLOCK / NPC;

	long int dist = write_packet_pointer - read_packet_pointer;

	if (dist <= NPACKET_PER_PC/2)
	//if (dist <= 2*NELEMENT_PER_PC*NPACKET_PER_BLOCK)
	{
		read_block_counter = read_block_counter;
	}
	else if ((dist >= NPACKET_PER_PC) && ((read_block_counter + 1) % NELEMENT == 0))
	{
		read_block_counter = read_block_counter + dist / (NPACKET_PER_BLOCK * NELEMENT_PER_PC * NCHANNEL) * (NELEMENT * NCHANNEL);
	}
	else
	{
		read_block_counter = read_block_counter + 1;
	}

	read_block_counter_s.write(read_block_counter);
}

void read_stream(
		hls::stream<packet> &in,
		hls::stream<ap_uint<DWIDTH>> databuffer[NPC],
		hls::stream<unsigned int> pc_packet_id_s[NPC],
		hls::stream<long int> &read_block_counter_s
)
{
#pragma HLS INLINE off

	unsigned int pc_id = 0;
	unsigned int pc_packet_id = 0;

	packet pkt;

	for (int k=0; k<NPACKET_PER_BLOCK; k++)
	{
#pragma HLS PIPELINE rewind

		in.read(pkt);

		unsigned long int packet_id = pkt.user(47, 0);
		unsigned int element_id = pkt.user(95, 64);
		unsigned int frequency_id = pkt.user(127, 96);

		if (k == 0)
		{
			pc_id = (element_id + frequency_id) % NPC; //
			unsigned int pc_element_id = element_id / NPC; //
			unsigned int pc_block_id = packet_id / NPACKET_PER_BLOCK; //
			unsigned int pc_block_packet_id = packet_id % NPACKET_PER_BLOCK; // pc_block_packet_id === k
			unsigned int pc_channel_id = frequency_id;

			pc_packet_id = pc_block_id * NCHANNEL * NELEMENT_PER_PC * NPACKET_PER_BLOCK +
					pc_channel_id * NELEMENT_PER_PC * NPACKET_PER_BLOCK +
					pc_element_id * NPACKET_PER_BLOCK; // + k

			// control
			control(pc_packet_id, read_block_counter_s);

			pc_packet_id %= NPACKET_PER_PC;

			pc_packet_id_s[pc_id].write(pc_packet_id);
		}

		databuffer[pc_id].write(pkt.data);
	}
}

void write_pc(
		hls::stream<ap_uint<DWIDTH>> &databuffer,
		hls::stream<unsigned int> &pc_packet_id_s,
		ap_uint<DWIDTH> *const pc
)
{
#pragma HLS INLINE

	//if (!pc_packet_id_s.empty() && !databuffer.empty())
	{
		unsigned int pc_packet_id = pc_packet_id_s.read();
		for (int k=0; k<NPACKET_PER_BLOCK; k++)
		{
#pragma HLS PIPELINE II=1 rewind
			pc[pc_packet_id + k] = databuffer.read();
		}
	}
}

void write_hbm(
		hls::stream<ap_uint<DWIDTH>> databuffer[NPC],
		hls::stream<unsigned int> pc_packet_id_s[NPC],
		ap_uint<DWIDTH> *const pc0,
		ap_uint<DWIDTH> *const pc1
)
{
#pragma HLS PIPELINE II=64
//#pragma HLS DATAFLOW
#pragma HLS INLINE off

	if (!pc_packet_id_s[0].empty() && !databuffer[0].empty())
		write_pc(databuffer[0], pc_packet_id_s[0], pc0);
	else if (!pc_packet_id_s[1].empty() && !databuffer[1].empty())
		write_pc(databuffer[1], pc_packet_id_s[1], pc1);
}

void stream2hbm(
		hls::stream<packet> &in,
		hls::stream<long int> &read_block_counter_s,
		ap_uint<DWIDTH> *pc0,
		ap_uint<DWIDTH> *pc1
)
{
#pragma HLS INLINE off
#pragma HLS DATAFLOW disable_start_propagation

	hls::stream<ap_uint<DWIDTH>, NPACKET_PER_BLOCK*2> databuffer[NPC];
#pragma HLS BIND_STORAGE variable=databuffer type=fifo impl=lutram
	hls::stream<unsigned int, NPACKET_PER_BLOCK*2> pc_packet_id_s[NPC];
#pragma HLS BIND_STORAGE variable=pc_packet_id_s type=fifo impl=lutram

	read_stream(in, databuffer, pc_packet_id_s, read_block_counter_s);
	write_hbm(databuffer, pc_packet_id_s, pc0, pc1);
}

void hbm2stream(
		hls::stream<packet> &out,
		hls::stream<long int> &read_block_counter_s,
		ap_uint<DWIDTH> *pc0,
		ap_uint<DWIDTH> *pc1
)
{
#pragma HLS INLINE off
#pragma HLS PIPELINE II=64
	static long int read_block_counter_pre = -1;
	long int read_block_counter_local = read_block_counter_s.read();
	if (read_block_counter_local == read_block_counter_pre)
	{
		return;
	}

	unsigned int frequency_id = read_block_counter_local / NELEMENT % NCHANNEL; //
	unsigned int element_id = read_block_counter_local % NELEMENT;
	unsigned int packet_id = read_block_counter_local / (NCHANNEL * NELEMENT) * NPACKET_PER_BLOCK; //

	unsigned int pc_id = (element_id + frequency_id) % NPC;
	unsigned int pc_block_id = read_block_counter_local / (NCHANNEL * NELEMENT);
	unsigned int pc_packet_id = pc_block_id * NCHANNEL * NELEMENT_PER_PC * NPACKET_PER_BLOCK + frequency_id * NELEMENT_PER_PC * NPACKET_PER_BLOCK + element_id / NPC * NPACKET_PER_BLOCK;
	pc_packet_id %= NPACKET_PER_PC;

	packet pkt;

	switch (pc_id)
	{
		case 0:
		{
			for (int k=0; k<NPACKET_PER_BLOCK; k++)
			{
#pragma HLS PIPELINE II=1 rewind
				pkt.data = pc0[pc_packet_id + k];
				pkt.user(63, 48) = 0;
				pkt.user(47, 0) = packet_id + k;
				pkt.user(95, 64) = element_id;
				pkt.user(127, 96) = frequency_id;
				out.write(pkt);
			}
		};break;
		case 1:
		{
			for (int k=0; k<NPACKET_PER_BLOCK; k++)
			{
#pragma HLS PIPELINE II=1 rewind
				pkt.data = pc1[pc_packet_id + k];
				pkt.user(63, 48) = 0;
				pkt.user(47, 0) = packet_id + k;
				pkt.user(95, 64) = element_id;
				pkt.user(127, 96) = frequency_id;
				out.write(pkt);
			}
		};break;
	}

	read_block_counter_pre = read_block_counter_local;
}

void krnl_reorder(
		hls::stream<packet> &in,
		hls::stream<packet> &out,
		ap_uint<DWIDTH> *pc0,
		ap_uint<DWIDTH> *pc1,
		int unused
)
{
#pragma HLS DATAFLOW disable_start_propagation
#pragma HLS INTERFACE mode=ap_ctrl_none port=return
#pragma HLS INTERFACE mode=m_axi bundle=axi0 depth=131072 max_read_burst_length=64 max_widen_bitwidth=512 max_write_burst_length=64 port=pc0 offset=off //NPACKET_PER_PC
#pragma HLS INTERFACE mode=m_axi bundle=axi1 depth=131072 max_read_burst_length=64 max_widen_bitwidth=512 max_write_burst_length=64 port=pc1 offset=off

	hls::stream<long int> read_block_counter_s;

	stream2hbm(in, read_block_counter_s, pc0, pc1);
	hbm2stream(out, read_block_counter_s, pc0, pc1);
}
