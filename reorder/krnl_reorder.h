#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>

#define DWIDTH 512

typedef ap_axiu<DWIDTH, 128, 0, 0> packet;

#define W 8

#define NCHANNEL 32
#define NELEMENT 32

#define NPC 2

#define NPACKET_PER_BLOCK 64

#define NPACKET_PER_PC (32*1024*32*16)//(8*1024*32*4) //(4*64*32*4)// (2*1024*32*4)// (256*32*4)

#define NSAMPLE_PER_PC (NPACKET_PER_PC * NPC / NCHANNEL / NELEMENT)

#define NELEMENT_PER_PC (NELEMENT/NPC)

typedef struct {
	ap_int<W> real;
	ap_int<W> imag;
} complex_t;

void krnl_reorder(
		hls::stream<packet> &in,
		hls::stream<packet> &out,
		ap_uint<DWIDTH> *pc0,
		ap_uint<DWIDTH> *pc1,
		int unused
);
