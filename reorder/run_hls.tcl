open_project -reset krnl_reorder
add_files "krnl_reorder.cpp krnl_reorder.h"
add_files -tb krnl_reorder_tb.cpp
set_top krnl_reorder
open_solution -reset solution1 -flow_target vitis
set_part {xcu55c-fsvh2892-2L-e}
create_clock -period 5
set_clock_uncertainty 0

csynth_design

export_design -format xo -output ../xo/krnl_reorder.xo
