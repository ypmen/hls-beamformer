#include "krnl_reorder.h"
#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <ctime>

#define NPACKET_PER_CHANNEL_ELEMENT 64
#define NSAMPLE_PER_PACKET (NPACKET_PER_BLOCK*64/2)

int main()
{
	//std::srand(std::time(nullptr));

	std::vector<ap_uint<DWIDTH>> pc0(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc1(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc2(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc3(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc4(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc5(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc6(NPACKET_PER_PC);
	std::vector<ap_uint<DWIDTH>> pc7(NPACKET_PER_PC);

	std::vector<complex_t> in((size_t)NPACKET_PER_CHANNEL_ELEMENT * NELEMENT * NCHANNEL * NSAMPLE_PER_PACKET);
	std::vector<complex_t> out((size_t)NPACKET_PER_CHANNEL_ELEMENT * NELEMENT * NCHANNEL * NSAMPLE_PER_PACKET);
	std::vector<complex_t> outRef((size_t)NPACKET_PER_CHANNEL_ELEMENT * NELEMENT * NCHANNEL * NSAMPLE_PER_PACKET);

	for (size_t ipacket=0; ipacket<NPACKET_PER_CHANNEL_ELEMENT; ipacket++)
	{
		for (size_t ielement=0; ielement<NELEMENT; ielement++)
		{
			for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
			{
				for (size_t isample=0; isample<NSAMPLE_PER_PACKET; isample++)
				{
					in[ipacket * NELEMENT * NCHANNEL * NSAMPLE_PER_PACKET +
					   ielement * NCHANNEL * NSAMPLE_PER_PACKET +
					   ichannel * NSAMPLE_PER_PACKET +
					   isample].real = ipacket+ielement+ichannel+isample;//std::rand()%256-128;
					in[ipacket * NELEMENT * NCHANNEL * NSAMPLE_PER_PACKET +
					   ielement * NCHANNEL * NSAMPLE_PER_PACKET +
					   ichannel * NSAMPLE_PER_PACKET +
					   isample].imag = 0;//std::rand()%256-128;
				}
			}
		}
	}

	for (size_t iblock=0; iblock<NPACKET_PER_CHANNEL_ELEMENT*NSAMPLE_PER_PACKET/(DWIDTH/8/2)/NPACKET_PER_BLOCK/2; iblock++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT; ielement++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					for (size_t k=0; k<DWIDTH/8/2; k++)
					{
						outRef[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].real = iblock + ielement + ichannel + (ipacket*(DWIDTH/8/2) + k);
						outRef[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].imag = 0;
					}
				}
			}
		}
	}

	hls::stream<packet> ins;
	hls::stream<packet> outs;

	packet pkt;
	ap_uint<DWIDTH> *pin = (ap_uint<DWIDTH> *)in.data();

	// random order
	int nseg = 8;
	int seglen = NPACKET_PER_CHANNEL_ELEMENT*NELEMENT*NCHANNEL/nseg;
	for (int k=0; k<nseg; k++)
	{
		std::vector<int> idx(seglen, 0);
		std::iota(idx.begin(), idx.end(), k*seglen);
		std::random_shuffle(idx.begin(), idx.end());
		for (auto i=idx.begin(); i!=idx.end(); ++i)
		{
			size_t ipacket = (*i) / (NELEMENT*NCHANNEL);
			size_t ielement = ((*i) %  (NELEMENT*NCHANNEL)) / NCHANNEL;
			size_t ichannel = ((*i) %  (NELEMENT*NCHANNEL)) % NCHANNEL;
			for (size_t isample=0; isample<NSAMPLE_PER_PACKET; isample += DWIDTH/8/2)
			{
				pkt.data = pin[((*i)*NSAMPLE_PER_PACKET+isample)/(DWIDTH/8/2)];
				pkt.user(63, 48) = 0;
				pkt.user(47, 0) = (ipacket*NSAMPLE_PER_PACKET+isample)/(DWIDTH/8/2);
				pkt.user(95, 64) = ielement;
				pkt.user(127, 96) = ichannel;
				ins.write(pkt);
			}
		}
	}

	// serial order
//	for (size_t ipacket=0; ipacket<NPACKET_PER_CHANNEL_ELEMENT; ipacket++)
//	{
//		for (size_t ielement=0; ielement<NELEMENT; ielement++)
//		{
//			for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
//			{
//				for (size_t isample=0; isample<NSAMPLE_PER_PACKET; isample += DWIDTH/8/2)
//				{
//					pkt.data = *pin;
//					pkt.user(63, 48) = 0;
//					pkt.user(47, 0) = (ipacket*NSAMPLE_PER_PACKET+isample)/(DWIDTH/8/2);
//					pkt.user(95, 64) = ielement;
//					pkt.user(127, 96) = ichannel;
//					ins.write(pkt);
//					pin++;
//				}
//			}
//		}
//	}

	int hist0;
	int hist1;
	int hist2;
	int hist3;
	int hist4;
	int hist5;
	int hist6;
	int hist7;
	int hist8;
	int hist9;
	int hist10;
	int hist11;
	int hist12;
	int hist13;
	int hist14;
	int hist15;
	int hist16;
	int hist17;
	int hist18;
	int hist19;
	int hist20;
	int hist21;
	int hist22;
	int hist23;
	int hist24;
	int hist25;
	int hist26;
	int hist27;
	int hist28;
	int hist29;
	int hist30;
	int hist31;

	for (size_t isample=0; isample<NPACKET_PER_CHANNEL_ELEMENT*NELEMENT*NCHANNEL*NSAMPLE_PER_PACKET; isample += NPACKET_PER_BLOCK*(DWIDTH/8/2))
	{
					krnl_reorder(ins, outs,
								 pc0.data(),
								 pc1.data(),
//								 pc2.data(),
//								 pc3.data(),
//								 pc4.data(),
//								 pc5.data(),
//								 pc6.data(),
//								 pc7.data(),
								 0
//								 hist0,
//								 hist1,
//								 hist2,
//								 hist3,
//								 hist4,
//								 hist5,
//								 hist6,
//								 hist7,
//								 hist8,
//								 hist9,
//								 hist10,
//								 hist11,
//								 hist12,
//								 hist13,
//								 hist14,
//								 hist15,
//								 hist16,
//								 hist17,
//								 hist18,
//								 hist19,
//								 hist20,
//								 hist21,
//								 hist22,
//								 hist23,
//								 hist24,
//								 hist25,
//								 hist26,
//								 hist27,
//								 hist28,
//								 hist29,
//								 hist30,
//								 hist31
								 );
	}

	ap_uint<DWIDTH> *pout = (ap_uint<DWIDTH> *)out.data();
	for (size_t iblock=0; iblock<NPACKET_PER_CHANNEL_ELEMENT/2; iblock++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT; ielement++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					outs.read(pkt);
					*pout = pkt.data;
					pout++;
				}
			}
		}
	}

	int errcnt = 0;
	for (size_t iblock=0; iblock<NPACKET_PER_CHANNEL_ELEMENT*NSAMPLE_PER_PACKET/(DWIDTH/8/2)/NPACKET_PER_BLOCK/2; iblock++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ielement=0; ielement<NELEMENT; ielement++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					for (size_t k=0; k<DWIDTH/8/2; k++)
					{
						if (outRef[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].real !=
						   out[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].real ||
						   outRef[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].imag !=
					       out[iblock * NCHANNEL * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ichannel * NELEMENT * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ielement * NPACKET_PER_BLOCK * (DWIDTH/8/2) +
							   ipacket * (DWIDTH/8/2) +
							   k].imag
						)
						{
							std::cout<<iblock<<" "<<ichannel<<" "<<ielement<<" "<<ipacket<<" "<<k<<std::endl;
							errcnt++;
						}
					}
				}
			}
		}
	}

	if (errcnt)
	{
		std::cout<<"Test failed!"<<" "<<errcnt<<std::endl;
		return 1;
	}
	else
	{
		std::cout<<"Test passed!"<<std::endl;
		return 0;
	}
}
