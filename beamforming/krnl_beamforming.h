#include <ap_int.h>
#include <hls_stream.h>
#include <ap_axi_sdata.h>

#define DWIDTH 512

typedef ap_axiu<DWIDTH, 128, 0, 0> packet;

#define W 8

#define NCHANNEL 32
#define NELEMENT 32
#define NBEAM 32

typedef struct {
	ap_int<W> real;
	ap_int<W> imag;
} complex_t;

typedef struct {
	ap_int<2*W+1> real;
	ap_int<2*W+1> imag;
} complex_out_t;

typedef struct {
	ap_uint<64> sample_id;
	ap_uint<16> frequency_id;
} user_t;

void krnl_beamforming(
		ap_uint<DWIDTH> *weight,
		hls::stream<packet> &in,
		hls::stream<packet> &out
);
