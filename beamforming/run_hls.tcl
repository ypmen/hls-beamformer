open_project -reset krnl_beamforming
add_files "krnl_beamforming.cpp krnl_beamforming.h"
add_files -tb krnl_beamforming_tb.cpp
set_top krnl_beamforming
open_solution -reset solution1 -flow_target vitis
set_part {xcu55c-fsvh2892-2L-e}
create_clock -period 5
set_clock_uncertainty 0


csynth_design

export_design -format xo -output ../xo/krnl_beamforming.xo
