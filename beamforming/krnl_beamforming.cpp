#include "krnl_beamforming.h"

template <class T>
T HLS_REG(T in)
{
#pragma HLS LATENCY max=2 min=2
#pragma HLS INLINE off
#pragma HLS PIPELINE
	return in;
}

void dsp48_mul_8s_8s_9s_17s_17s(
                ap_int<8> a0,
                ap_int<8> a1,
                ap_int<9> b,
                ap_int<17> &c0,
                ap_int<17> &c1
)
{
#pragma HLS INLINE
        ap_int<34> c = (a0 + ((ap_int<25>)a1 << 17)) * b;
        c0 = c.range(16, 0);
        c1 = c.range(33, 17) + c[16];
}

void dsp48_mul_9s_9s_8s_17s_17s(
                ap_int<9> a0,
                ap_int<9> a1,
                ap_int<8> b,
                ap_int<17> &c0,
                ap_int<17> &c1
)
{
#pragma HLS INLINE
        ap_int<34> c = (a0 + ((ap_int<26>)a1 << 17)) * b;
        c0 = c.range(16, 0);
        c1 = c.range(33, 17) + c[16];
}

void complex_mul(
                complex_t a0,
                complex_t a1,
                complex_t b,
                complex_out_t &c0,
                complex_out_t &c1
)
{
#pragma HLS INLINE

        ap_int<2*W+1> k1_0, k1_1;
        dsp48_mul_9s_9s_8s_17s_17s(a0.real+a0.imag, a1.real+a1.imag, b.real, k1_0, k1_1);

        ap_int<2*W+1> k2_0, k2_1;
        dsp48_mul_8s_8s_9s_17s_17s(a0.real, a1.real, b.imag-b.real, k2_0, k2_1);

        ap_int<2*W+1> k3_0, k3_1;
        dsp48_mul_8s_8s_9s_17s_17s(a0.imag, a1.imag, b.real+b.imag, k3_0, k3_1);

        c0.real = k1_0 - k3_0;
        c0.imag = k1_0 + k2_0;

        c1.real = k1_1 - k3_1;
        c1.imag = k1_1 + k2_1;
}

void gemv(
		complex_t C[NBEAM*(DWIDTH/(2*W)/NELEMENT)],
		complex_t A[NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT],
		complex_t B[NELEMENT]
)
{
#pragma HLS INLINE
	for (int j=0; j<NBEAM*(DWIDTH/(2*W)/NELEMENT); j+=2)
	{
		ap_int<2*W+6> sum0_r = 0, sum1_r = 0; //2*W+1+log2(nelement)
		ap_int<2*W+6> sum0_i = 0, sum1_i = 0;
		for (int i=0; i<NELEMENT; i++)
		{
			complex_out_t tmp0, tmp1;
			complex_mul(A[j][i], A[j+1][i], B[i], tmp0, tmp1);
			sum0_r += tmp0.real;
			sum0_i += tmp0.imag;
			sum1_r += tmp1.real;
			sum1_i += tmp1.imag;
		}
		C[j].real = sum0_r.range(2*W+5, 2*W-2);
		C[j].imag = sum0_i.range(2*W+5, 2*W-2);
		C[j+1].real = sum1_r.range(2*W+5, 2*W-2);
		C[j+1].imag = sum1_i.range(2*W+5, 2*W-2);
	}
}

void read_B(
		hls::stream<complex_t> streamB[NELEMENT],
		complex_t B[NELEMENT]
)
{
#pragma HLS INLINE
	for (int k=0; k<NELEMENT; k++)
	{
		B[k] = streamB[k].read();
	}
}

void write_C(
		complex_t C[NBEAM],
		hls::stream<complex_t> streamC[NBEAM]
)
{
#pragma HLS INLINE
	for (int k=0; k<NBEAM; k++)
	{
		streamC[k].write(C[k]);
	}
}

void read_weight2(
		complex_t A[NCHANNEL][NELEMENT/(DWIDTH/(2*W))][NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT],
		complex_t regA[NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT],
		unsigned int ichannel
)
{
#pragma HLS INLINE

	for (int ibeam_j=0; ibeam_j<NELEMENT/(DWIDTH/(2*W)); ibeam_j++)
	{
		for (int ibeam_i=0; ibeam_i<NBEAM*(DWIDTH/(2*W)/NELEMENT); ibeam_i++)
		{
			for (int ielement=0; ielement<NELEMENT; ielement++)
			{
				regA[ibeam_i][ielement] = A[ichannel][ibeam_j][ibeam_i][ielement];
			}
		}
	}
}

void read_weight1(
		ap_uint<DWIDTH> *weight,
		complex_t A[NCHANNEL][NELEMENT/(DWIDTH/(2*W))][NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT]
)
{
#pragma HLS INLINE

	static unsigned int k = 0;

	static unsigned int count = 0;
	if (count++ % (32 * NCHANNEL * NBEAM) < NCHANNEL * NBEAM)
	{
		unsigned int ichannel = k / NBEAM % NCHANNEL;
		unsigned int ibeam = k % NBEAM;
		unsigned int ibeam_j = ibeam / (NBEAM*(DWIDTH/(2*W)/NELEMENT));
		unsigned int ibeam_i = ibeam % (NBEAM*(DWIDTH/(2*W)/NELEMENT));

		ap_uint<NELEMENT*2*W> temp;
		for (int j=0; j<NELEMENT/(DWIDTH/(2*W)); j++)
		{
			temp(j*DWIDTH+DWIDTH-1, j*DWIDTH) = weight[ichannel * NBEAM * NELEMENT/(DWIDTH/(2*W)) + ibeam * NELEMENT/(DWIDTH/(2*W)) + j];
		}

		for (int ielement=0; ielement<NELEMENT; ielement++)
		{
			A[ichannel][ibeam_j][ibeam_i][ielement].real = temp(ielement*2*W+W-1, ielement*2*W);
			A[ichannel][ibeam_j][ibeam_i][ielement].imag = temp(ielement*2*W+2*W-1, ielement*2*W+W);
		}

		k++;
	}
	else
	{
		k = 0;
	}
}

void read_weight(
		ap_uint<DWIDTH> *weight,
		complex_t regA[NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT],
		unsigned int ichannel
)
{
#pragma HLS INLINE

	static complex_t A[NCHANNEL][NELEMENT/(DWIDTH/(2*W))][NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT];
#pragma HLS AGGREGATE compact=auto variable=A
#pragma HLS BIND_STORAGE variable=A type=ram_2p impl=bram
	#pragma HLS ARRAY_PARTITION dim=3 type=complete variable=A
	#pragma HLS ARRAY_RESHAPE dim=4 type=complete variable=A

	read_weight1(weight, A);
	read_weight2(A, regA, ichannel);
}

void beamforming(
		ap_uint<DWIDTH> *weight,
		hls::stream<complex_t> streamB[NELEMENT],
		hls::stream<complex_t> streamC[NBEAM],
		hls::stream<user_t> &user_s1,
		hls::stream<user_t> &user_s2
)
{
#pragma HLS PIPELINE II=1
#pragma HLS INLINE off
	complex_t A[NBEAM*(DWIDTH/(2*W)/NELEMENT)][NELEMENT];
#pragma HLS ARRAY_PARTITION dim=1 type=complete variable=A
#pragma HLS ARRAY_PARTITION dim=2 type=complete variable=A

	complex_t B[NELEMENT];
#pragma HLS ARRAY_PARTITION dim=1 type=complete variable=B

	complex_t C[NELEMENT];
#pragma HLS ARRAY_PARTITION dim=1 type=complete variable=C

	user_t user = user_s1.read();
	user_s2.write(user);

    unsigned int ichannel = user.frequency_id;

	read_B(streamB, B);
	for (int k=0; k<NELEMENT/(DWIDTH/(2*W)); k++)
	{
		read_weight(weight, A, ichannel);
		gemv(C, A, B);
	}
	write_C(C, streamC);
}

void read_stream(
		hls::stream<packet> &in,
		hls::stream<complex_t> streamB[NELEMENT],
		hls::stream<user_t> &user_s
)
{
#pragma HLS INLINE off
	for (int k=0; k<NELEMENT/(DWIDTH/(2*W)); k++)
	{
#pragma HLS PIPELINE II=1 rewind
		packet pkt;
		in.read(pkt);

		if (k==0)
		{
			user_t user;
			user.sample_id = pkt.user(63, 0);
			user.frequency_id = pkt.user(127, 96);
			user_s.write(user);
		}

		for (int i=0; i<DWIDTH/(2*W); i++)
		{
			complex_t data;
			data.real = pkt.data(i*2*W+W-1, i*2*W);
			data.imag = pkt.data(i*2*W+2*W-1, i*2*W+W);
			streamB[k * DWIDTH/(2*W) + i].write(data);
		}
	}
}

void write_stream(
		hls::stream<complex_t> streamC[NBEAM],
		hls::stream<user_t> &user_s,
		hls::stream<packet> &out
)
{
#pragma HLS INLINE off
#pragma HLS PIPELINE II=1
	packet pkt;

	user_t user = user_s.read();
	pkt.user(63, 0) = user.sample_id;
	pkt.user(127, 96) = user.frequency_id;

	for (int k=0; k<NBEAM/(DWIDTH/(2*W)); k++)
	{

		for (int i=0; i<DWIDTH/(2*W); i++)
		{
			complex_t data = streamC[k * DWIDTH/(2*W) + i].read();

			pkt.data(i*2*W+W-1, i*2*W) = data.real;
			pkt.data(i*2*W+2*W-1, i*2*W+W) = data.imag;
		}

		out.write(pkt);
	}
}

void krnl_beamforming(
		ap_uint<DWIDTH> *weight,
		hls::stream<packet> &in,
		hls::stream<packet> &out
)
{
#pragma HLS INTERFACE mode=ap_ctrl_none port=return
#pragma HLS DATAFLOW disable_start_propagation

	hls::stream<complex_t> streamB[NELEMENT];
	hls::stream<complex_t> streamC[NBEAM];
	hls::stream<user_t> user_s1, user_s2, user_s3;

	read_stream(in, streamB, user_s1);
	beamforming(weight, streamB, streamC, user_s1, user_s2);
	write_stream(streamC, user_s2, out);
}
