#include <iostream>
#include <fstream>
#include <ctime>
#include <math.h>

#include "krnl_beamforming.h"

#define NPACKET_PER_ELEMENT_CHANNEL 1//4

#define NSAMPLE_PER_PACKET 2048//32

int main()
{
	size_t N = (size_t)NPACKET_PER_ELEMENT_CHANNEL*NCHANNEL*NSAMPLE_PER_PACKET;
	complex_t *in = new complex_t [N*NELEMENT];
	complex_t *weight = new complex_t [NCHANNEL*NBEAM*NELEMENT];
	complex_t *out = new complex_t [N*NBEAM];
	complex_t *outRef = new complex_t [N*NBEAM];

	std::ifstream infile_in;
	//infile_in.open("/homes/ypmen/vitis/beamformer/hls/beamforming/src/input.dat", std::ios::binary);
	infile_in.open("/.aux_mnt/fpgdeva/ypmen/vitis/beamformer/python/tmp/input.dat", std::ios::binary);
	infile_in.read((char *)in, sizeof(complex_t)*N*NELEMENT);
	infile_in.close();

	std::ifstream infile_weight;
	//infile_weight.open("/homes/ypmen/vitis/beamformer/hls/beamforming/src/weight.dat", std::ios::binary);
	infile_weight.open("/.aux_mnt/fpgdeva/ypmen/vitis/beamformer/python/tmp/weight.dat", std::ios::binary);
	infile_weight.read((char *)weight, sizeof(complex_t)*NCHANNEL*NBEAM*NELEMENT);
	infile_weight.close();

	std::ifstream infile_out;
	//infile_out.open("/homes/ypmen/vitis/beamformer/hls/beamforming/src/output.dat", std::ios::binary);
	infile_out.open("/.aux_mnt/fpgdeva/ypmen/vitis/beamformer/python/tmp/output.dat", std::ios::binary);
	infile_out.read((char *)outRef, sizeof(complex_t)*N*NBEAM);
	infile_out.close();

	hls::stream<packet> ins, outs;

	packet pkt;
	ap_uint<DWIDTH> *pin = (ap_uint<DWIDTH> *)in;
	for (size_t k=0; k<N*NELEMENT; k+= DWIDTH/(2*W))
	{
		unsigned int frequency_id = k / (NSAMPLE_PER_PACKET*NELEMENT) % NCHANNEL;
		unsigned long int sample_id = k / (NCHANNEL*NSAMPLE_PER_PACKET*NELEMENT) * NSAMPLE_PER_PACKET + k / (NELEMENT * DWIDTH/(2*W)) % NSAMPLE_PER_PACKET;
		pkt.data = *pin;
		pkt.user(63, 0) = sample_id;
		pkt.user(95, 64) = 0;
		pkt.user(127, 96) = frequency_id;
		ins.write(pkt);
		pin++;
	}

	for (size_t k=0; k<N; k++)
	{
		krnl_beamforming((ap_uint<DWIDTH> *)weight, ins, outs);
	}

	ap_uint<DWIDTH> *pout = (ap_uint<DWIDTH> *)out;
	for (size_t k=0; k<N*NBEAM; k+= DWIDTH/(2*W))
	{
		unsigned int frequency_id = k / (NSAMPLE_PER_PACKET*NELEMENT) % NCHANNEL;
		unsigned long int sample_id = k / (NCHANNEL*NSAMPLE_PER_PACKET*NELEMENT) * NSAMPLE_PER_PACKET + k / (NELEMENT * DWIDTH/(2*W)) % NSAMPLE_PER_PACKET;

		outs.read(pkt);
		*pout = pkt.data;

		if (pkt.user(63, 0).to_int64() != sample_id or pkt.user(127, 96).to_int() != frequency_id)
		{
			std::cout<<"Test failed!"<<" user data error"<<std::endl;
			return 1;
		}
		pout++;
	}

	int errcnt = 0;

	for (size_t k=NCHANNEL*NBEAM; k<N; k++)
	{
		std::cout<<k<<std::endl;
		for (int ibeam=0; ibeam<NBEAM; ibeam++)
		{
			if (std::abs(out[k * NBEAM + ibeam].real-outRef[k * NBEAM + ibeam].real) > 1 or std::abs(out[k * NBEAM + ibeam].imag-outRef[k * NBEAM + ibeam].imag) > 1)
			{
				errcnt += 1;
			}
		}
	}

	if (errcnt)
	{
		std::cout<<"Test failed!"<<" "<<errcnt<<std::endl;
		return 1;
	}
	else
	{
		std::cout<<"Test passed!"<<std::endl;
		return 0;
	}
}
