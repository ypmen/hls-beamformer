#include "corner_turner2.h"

int main()
{
	std::vector<complex_t> in(NCHANNEL * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET * NBEAM);

	for (int ichannel=0; ichannel<NCHANNEL; ichannel++)
	{
		for (int isample=0; isample<NPACKET_PER_BLOCK*NSAMPLE_PER_PACKET; isample+=NSAMPLE_PER_PACKET)
		{
			for (int ibeam=0; ibeam<NBEAM; ibeam++)
			{
				unsigned int ipacket = (ichannel * NBEAM * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET + ibeam * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET + isample) / NSAMPLE_PER_PACKET;
				for (int k=0; k<NSAMPLE_PER_PACKET; k++)
				{
					if (k < 2)
					{
						in[ichannel * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET * NBEAM + (isample + k) * NBEAM + ibeam].real = (ipacket >> (k * 2 * W)) & 0xFF;
						in[ichannel * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET * NBEAM + (isample + k) * NBEAM + ibeam].imag = (ipacket >> (k * 2 * W + W)) & 0xFF;
					}
					else
					{
						in[ichannel * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET * NBEAM + (isample + k) * NBEAM + ibeam].real = 0;
						in[ichannel * NPACKET_PER_BLOCK * NSAMPLE_PER_PACKET * NBEAM + (isample + k) * NBEAM + ibeam].imag = 0;
					}
				}
			}
		}
	}

	hls::stream<packet> ins;
	hls::stream<packet> outs;

	packet pkt;
	ap_uint<DWIDTH> *pin = (ap_uint<DWIDTH> *)in.data();

	for (int ichannel=0; ichannel<NCHANNEL; ichannel++)
	{
		for (int isample=0; isample<NPACKET_PER_BLOCK*NSAMPLE_PER_PACKET; isample++)
		{
			for (int ibeam=0; ibeam<NBEAM; ibeam += NSAMPLE_PER_PACKET)
			{
				pkt.data = *pin;
				pkt.user(63, 0) = isample;
				pkt.user(127, 96) = ichannel;
				ins.write(pkt);
				pin++;
			}
		}
	}

	for (int ichannel=0; ichannel<NCHANNEL; ichannel++)
	{
		for (int isample=0; isample<NPACKET_PER_BLOCK*NSAMPLE_PER_PACKET; isample++)
		{
			for (int ibeam=0; ibeam<NBEAM; ibeam += NSAMPLE_PER_PACKET)
			{
				krnl_corner_turner2(ins, outs);
			}
		}
	}

	size_t errcnt = 0;

	for (int ichannel=0; ichannel<NCHANNEL/2; ichannel++)
	{
		for (int ibeam=0; ibeam<NBEAM; ibeam++)
		{
			for (int ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
			{
				unsigned int k = ichannel * NBEAM * NPACKET_PER_BLOCK + ibeam * NPACKET_PER_BLOCK + ipacket;

				outs.read(pkt);
				unsigned int packet_id = pkt.user(63, 0);
				unsigned int beam_id = pkt.user(95, 64);
				unsigned int frequency_id = pkt.user(127, 96);
				if (pkt.data != k or packet_id != ipacket or beam_id != ibeam or frequency_id != ichannel)
				{
					errcnt++;
				}
			}
		}
	}

	if (errcnt)
	{
		std::cout<<"Test failed!"<<" "<<errcnt<<std::endl;
		return 1;
	}
	else
	{
		std::cout<<"Test passed!"<<std::endl;
		return 0;
	}
}
