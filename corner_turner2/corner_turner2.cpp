#include "corner_turner2.h"

void push(
	hls::stream<packet> &in,
	ap_uint<2*W> ram_data[NSAMPLE_PER_PACKET][RAM_DEPTH],
	user_t ram_user[RAM_DEPTH]
)
{
#pragma HLS INLINE

	static unsigned int count = 0;

	packet pkt;
	in.read(pkt);

	unsigned int sample_id = pkt.user(63, 0);
	unsigned int frequency_id =  pkt.user(127, 96);

	unsigned int ihalf = count / (NBEAM * NPACKET_PER_BLOCK) % 2;
	unsigned int ipacket = count / (NBEAM / NSAMPLE_PER_PACKET) / NSAMPLE_PER_PACKET % NPACKET_PER_BLOCK;
	unsigned int ibeam_n = count % (NBEAM / NSAMPLE_PER_PACKET);

	unsigned int col0 = ihalf * (RAM_DEPTH / 2) + ipacket * NBEAM + ibeam_n * NSAMPLE_PER_PACKET;
	unsigned int row0 = (sample_id + col0) % NSAMPLE_PER_PACKET;

	unsigned int idx = col0 + sample_id % NSAMPLE_PER_PACKET;
	ram_user[idx].packet_id = sample_id / NSAMPLE_PER_PACKET;
	ram_user[idx].beam_id = ibeam_n * NSAMPLE_PER_PACKET + sample_id % NSAMPLE_PER_PACKET;
	ram_user[idx].frequency_id = frequency_id;

	for (int k=0; k<NSAMPLE_PER_PACKET; k++)
	{
		unsigned int icol = col0 + k;
		unsigned int irow = (sample_id + k) % NSAMPLE_PER_PACKET;

		ram_data[irow][icol] = pkt.data(k*2*W+2*W-1, k*2*W);
	}

	count++;
}

void pop(
	ap_uint<2*W> ram_data[NSAMPLE_PER_PACKET][RAM_DEPTH],
	user_t ram_user[RAM_DEPTH],
	hls::stream<packet> &out
)
{
#pragma HLS INLINE

	static unsigned int boot_count = 0;
	static unsigned int count = 0;
	if (boot_count < NBEAM*NPACKET_PER_BLOCK)
	{
		boot_count++;
		return;
	}

	unsigned int ihalf = count / (NBEAM * NPACKET_PER_BLOCK) % 2;
	unsigned int icol = ihalf * (RAM_DEPTH / 2) + count % NPACKET_PER_BLOCK * NBEAM + count / NPACKET_PER_BLOCK % NBEAM;

	unsigned int beam_id = ram_user[icol].beam_id;
	unsigned long int packet_id = ram_user[icol].packet_id;
	unsigned int frequency_id = ram_user[icol].frequency_id;

	packet pkt;
	pkt.keep = -1;
	pkt.user(63, 0) = packet_id;
	pkt.user(95, 64) = beam_id;
	pkt.user(127, 96) = frequency_id;

	for (int k=0; k<NSAMPLE_PER_PACKET; k++)
	{
		unsigned int idx = (icol % NSAMPLE_PER_PACKET + k) % NSAMPLE_PER_PACKET;
		pkt.data(k*2*W+2*W-1, k*2*W) = ram_data[idx][icol];
	}

	out.write(pkt);

	count++;
}

void krnl_corner_turner2(
	hls::stream<packet> &in,
	hls::stream<packet> &out
)
{
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS PIPELINE II=1

	static ap_uint<2*W> ram_data[NSAMPLE_PER_PACKET][RAM_DEPTH];
#pragma HLS ARRAY_PARTITION dim=1 type=complete variable=ram_data
	static user_t ram_user[RAM_DEPTH];

	push(in, ram_data, ram_user);
	pop(ram_data, ram_user, out);
}
