open_project -reset corner_turner2
add_files "corner_turner2.cpp corner_turner2.h"
add_files -tb corner_turner2_tb.cpp
set_top krnl_corner_turner2
open_solution -reset solution1 -flow_target vitis
set_part {xcu55c-fsvh2892-2L-e}
create_clock -period 5
set_clock_uncertainty 0


csynth_design

export_design -format xo -output ../xo/krnl_corner_turner2.xo
