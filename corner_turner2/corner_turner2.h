#include <hls_stream.h>
#include <ap_axi_sdata.h>

#define DWIDTH 512

typedef ap_axiu<DWIDTH, 128, 0, 0> packet;

#define W 8

#define NCHANNEL 32
#define NBEAM 32

#define NPACKET_PER_BLOCK 64

#define NSAMPLE_PER_PACKET (DWIDTH/(2*W))
#define RAM_DEPTH (2*NPACKET_PER_BLOCK*NBEAM)

typedef struct {
	ap_int<W> real;
	ap_int<W> imag;
} complex_t;

typedef struct {
	ap_uint<64> packet_id;
	ap_uint<16> beam_id;
	ap_uint<16> frequency_id;
} user_t;

void krnl_corner_turner2(
	hls::stream<packet> &in,
	hls::stream<packet> &out
);
