open_project -reset spead_send
add_files "spead_send.cpp spead_send.h"
add_files -tb spead_send_tb.cpp
set_top krnl_spead_send
open_solution -reset solution1 -flow_target vitis
set_part {xcu55c-fsvh2892-2L-e}
create_clock -period 5
set_clock_uncertainty 0


csynth_design

export_design -format xo -output ../xo/krnl_spead_send.xo
