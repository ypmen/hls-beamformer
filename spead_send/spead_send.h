#include <ap_axi_sdata.h>
#include <hls_stream.h>

#define DWIDTH 512
#define W 8

typedef ap_axiu<DWIDTH, 128, 0, 0> pkt_pol;
typedef ap_axiu<DWIDTH, 32, 8, 0> pkt_gbe;

#define NCHANNEL 32
#define NBEAM 32

#define NPACKET_PER_BLOCK 64

#define PACKET_LENGTH (NPACKET_PER_BLOCK*(DWIDTH/W))

#define SPEAD_HEADER 0x530402060000000b

#define ITEM_ID_TIMESTAMP 0x8016
#define ITEM_ID_CLIPPING_CNT 0x8017
#define ITEM_ID_ERROR_VECTOR 0x8018
#define ITEM_ID_SCALE_FACTOR 0x8019
#define ITEM_ID_ORDER_VECTOR 0x8020
#define ITEM_ID_RESERVED 0x8021
#define ITEM_ID_PAYLOAD 0x8022

#define ITEMID_NULL 0x8000

void krnl_spead_send(
		hls::stream<pkt_pol> &in,
		hls::stream<pkt_gbe> &out,
		unsigned int &hardware_id,
		bool &reset
);
