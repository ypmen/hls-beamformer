#include "spead_send.h"

void map(
		ap_uint<128> &user,
		ap_uint<48> &timestamp,
		ap_uint<48> &heap_id,
		ap_uint<16> &beam_id,
		ap_uint<16> &channel_id
)
{
#pragma HLS INLINE

	ap_uint<64> ipacket = user(63, 0);
	ap_uint<32> ibeam = user(95, 64);
	ap_uint<32> ichannel = user(127, 96);

	timestamp = ipacket * DWIDTH / (2 * W);
	beam_id = ichannel % 2 * NBEAM + ibeam;
	channel_id = ichannel / 2;
	heap_id = channel_id * NBEAM * 2 + beam_id + 1;
}

ap_uint<64> byte_reverse(ap_uint<64> a)
{
#pragma HLS INLINE

	ap_uint<64> b;

	b(7, 0) = a(63, 56);
	b(15, 8) = a(55, 48);
	b(23, 16) = a(47, 40);
	b(31, 24) = a(39, 32);
	b(39, 32) = a(31, 24);
	b(47, 40) = a(23, 16);
	b(55, 48) = a(15, 8);
	b(63, 56) = a(7, 0);

	return b;
}

void krnl_spead_send(
		hls::stream<pkt_pol> &in,
		hls::stream<pkt_gbe> &out,
		unsigned int &hardware_id,
		bool &reset
){
#pragma HLS INTERFACE axis port = in
#pragma HLS INTERFACE axis port = out
#pragma HLS INTERFACE s_axilite port = hardware_id bundle = control
#pragma HLS INTERFACE s_axilite port = reset bundle = control
#pragma HLS INTERFACE mode=ap_ctrl_none port=return
#pragma HLS PIPELINE II=1

	static unsigned int counter = 0;

	static ap_uint<64> spead_header = byte_reverse((ap_uint<64>)SPEAD_HEADER);
	static ap_uint<64> item_heap_id = byte_reverse(((ap_uint<64>)0x8001 << 48) + 0);
	static ap_uint<64> item_heap_size = byte_reverse(((ap_uint<64>)0x8002 << 48) + PACKET_LENGTH);
	static ap_uint<64> item_heap_offset = byte_reverse(((ap_uint<64>)0x8003 << 48) + 0);
	static ap_uint<64> item_packet_payload_length = byte_reverse(((ap_uint<64>)0x8004 << 48) + PACKET_LENGTH);
	static ap_uint<64> item_timestamp = byte_reverse(((ap_uint<64>)ITEM_ID_TIMESTAMP << 48) + 0);
	static ap_uint<64> item_clipping_cnt = byte_reverse(((ap_uint<64>)ITEM_ID_CLIPPING_CNT << 48) + 0);
	static ap_uint<64> item_error_vector = byte_reverse(((ap_uint<64>)ITEM_ID_ERROR_VECTOR << 48) + 0);
	static ap_uint<64> item_scale_vector = byte_reverse(((ap_uint<64>)ITEM_ID_SCALE_FACTOR << 48) + 0);
	static ap_uint<64> item_order_vector = byte_reverse(((ap_uint<64>)ITEM_ID_ORDER_VECTOR << 48) + 0);

	static pkt_pol word_in;
	static pkt_gbe word_out;

	static ap_uint<DWIDTH> data_cache;

	static ap_uint<48> timestamp= 0;
	static ap_uint<16> beam_id = 0;
	static ap_uint<16> channel_id = 0;
	static ap_uint<48> heap_id = 0;

	if (reset)
	{
		counter = 0;

		item_heap_id = byte_reverse(((ap_uint<64>)0x8001 << 48) + 0);
		item_heap_offset = byte_reverse(((ap_uint<64>)0x8003 << 48) + 0);
		item_timestamp = byte_reverse(((ap_uint<64>)ITEM_ID_TIMESTAMP << 48) + 0);
		item_clipping_cnt = byte_reverse(((ap_uint<64>)ITEM_ID_CLIPPING_CNT << 48) + 0);
		item_error_vector = byte_reverse(((ap_uint<64>)ITEM_ID_ERROR_VECTOR << 48) + 0);
		item_scale_vector = byte_reverse(((ap_uint<64>)ITEM_ID_SCALE_FACTOR << 48) + 0);
		item_order_vector = byte_reverse(((ap_uint<64>)ITEM_ID_ORDER_VECTOR << 48) + 0);
	}
	else
	{
		if (counter != 1 && counter != PACKET_LENGTH/(DWIDTH/8)+1)
		{
			in.read(word_in);

			map(word_in.user, timestamp, heap_id, beam_id, channel_id);
		}

		// dump spead header
		if (counter == 0)
		{
			word_out.data(63, 0) = byte_reverse(spead_header);
			word_out.data(127, 64) = byte_reverse(item_heap_id + heap_id);
			word_out.data(191, 128) = byte_reverse(item_heap_size);
			word_out.data(255, 192) = byte_reverse(item_heap_offset);
			word_out.data(319, 256) = byte_reverse(item_packet_payload_length);
			word_out.data(383, 320) = byte_reverse(item_timestamp + timestamp);
			word_out.data(447, 384) = byte_reverse(item_clipping_cnt);
			word_out.data(511, 448) = byte_reverse(item_error_vector);

			word_out.keep = -1;
			word_out.last = 0;

			out.write(word_out);

			counter++;
		}
		else if (counter == 1)
		{
			ap_uint<48> order_vector;
			order_vector(15, 0) = beam_id;
			order_vector(31, 16) = channel_id;
			order_vector(47, 32) = 0;

			word_out.data(63, 0) = byte_reverse(item_scale_vector);
			word_out.data(127, 64) = byte_reverse(item_order_vector + order_vector);

			word_out.data(511, 256) = data_cache(255, 0);

			word_out.keep = -1;
			word_out.last = 0;

			out.write(word_out);

			counter++;
		}
		// dump data
		else if (counter == PACKET_LENGTH/(DWIDTH/8)+1)
		{
			word_out.data(255, 0) = data_cache(511, 256);

			word_out.keep(31, 0) = -1;
			word_out.keep(63, 32) = 0;
			word_out.last = 1;

			out.write(word_out);

			counter = 0;
		}
		else
		{
			word_out.data(255, 0) = data_cache(511, 256);
			word_out.data(511, 256) = word_in.data(255, 0);

			word_out.keep = -1;
			word_out.last = 0;

			out.write(word_out);

			counter++;
		}

		data_cache = word_in.data;
	}
}
