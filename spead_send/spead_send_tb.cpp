#include <iostream>

#include "spead_send.h"

int main()
{
	hls::stream<pkt_pol> in;
	hls::stream<pkt_gbe> out;

	for (size_t k=0; k<4; k++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ibeam=0; ibeam<NBEAM; ibeam++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					pkt_pol pkt;
					pkt.data = ichannel * NBEAM * NPACKET_PER_BLOCK + ibeam * NPACKET_PER_BLOCK + ipacket;
					pkt.user(63, 0) = k * NPACKET_PER_BLOCK + ipacket;
					pkt.user(95, 64) = ibeam;
					pkt.user(127, 96) = ichannel;
					in.write(pkt);
				}
			}
		}
	}

	unsigned int hardware_id = 0;
	bool reset = 0;

	for (size_t k=0; k<4; k++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ibeam=0; ibeam<NBEAM; ibeam++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK+2; ipacket++)
				{
					krnl_spead_send(in, out, hardware_id, reset);
				}
			}
		}
	}

	for (size_t k=0; k<4; k++)
	{
		for (size_t ichannel=0; ichannel<NCHANNEL; ichannel++)
		{
			for (size_t ibeam=0; ibeam<NBEAM; ibeam++)
			{
				for (size_t ipacket=0; ipacket<NPACKET_PER_BLOCK; ipacket++)
				{
					pkt_gbe pkt;

					if (ipacket == 0)
					{
						out.read(pkt);
						ap_uint<48> heap_offset = pkt.data(239, 192);
						ap_uint<48> timestamp = pkt.data(367, 320);
						out.read(pkt);
						ap_uint<48> order_vector = pkt.data(111, 64);
						ap_uint<16> beam_id = order_vector(15, 0);
						ap_uint<16> channel_id = order_vector(31, 16);

						assert(beam_id ==  ichannel % 2 * NBEAM + ibeam);
						assert(channel_id == ichannel / 2);
						assert(timestamp == k * NPACKET_PER_BLOCK * DWIDTH / (2 * W));
						assert(heap_offset == 0);
					}

					out.read(pkt);
				}
			}
		}
	}
}
